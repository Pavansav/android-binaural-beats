package com.github.axet.binauralbeats.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.github.axet.binauralbeats.beats.BinauralBeatVoice;
import com.github.axet.binauralbeats.beats.Period;
import com.github.axet.binauralbeats.beats.Program;
import com.github.axet.binauralbeats.beats.SoundLoop;

import java.util.Iterator;

/**
 * Line Graph View. This draws a line chart.
 *
 * @author jjoe64 - jonas gehring - http://www.jjoe64.com
 *         <p>
 *         Copyright (C) 2011 Jonas Gehring
 *         Licensed under the GNU Lesser General Public License (LGPL)
 *         http://www.gnu.org/licenses/lgpl.html
 */
public class LineGraphView extends GraphView {
    private Paint paintBackground;
    private boolean drawBackground;
    private float backgroundLimit = 100000000000000f; // Only draw background until this point
    Program pr;

    public LineGraphView(Context context) {
        super(context);
    }

    public LineGraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LineGraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LineGraphView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public LineGraphView(Context context, String title) {
        super(context, title);
    }

    void create() {
        super.create();

        paintBackground = new Paint();
        paintBackground.setARGB(255, 20, 40, 60);
        paintBackground.setStrokeWidth(4);

        if (isInEditMode()) {
            Program pr = new Program("Test");

            pr.addPeriod(new Period(120, SoundLoop.NONE, 0.2f).
                    addVoice(new BinauralBeatVoice(20f, 70f, 0.65f)).
                    addVoice(new BinauralBeatVoice(20f, 50f, 0.55f)));

            for (int i = 0; i < 8; i++)
                pr.addPeriod(new Period(20, SoundLoop.NONE, 0.2f).
                        addVoice(new BinauralBeatVoice(70f, 70f, 0.65f)).
                        addVoice(new BinauralBeatVoice(70f, 50f, 0.55f)))
                        .addPeriod(new Period(10, SoundLoop.NONE, 0.2f).
                                addVoice(new BinauralBeatVoice(8f, 8f, 0.65f)).
                                addVoice(new BinauralBeatVoice(50f, 70f, 0.55f)));

            pr.addPeriod(new Period(120, SoundLoop.NONE, 0.2f).
                    addVoice(new BinauralBeatVoice(70f, 10f, 0.65f)).
                    addVoice(new BinauralBeatVoice(50f, 10f, 0.55f)).
                    addVoice(new BinauralBeatVoice(3.7f, 3.7f, 0.4f)).
                    addVoice(new BinauralBeatVoice(2.5f, 2.5f, 0.4f)).
                    addVoice(new BinauralBeatVoice(5.9f, 5.9f, 0.4f)));
            load(pr);
        }
    }

    @Override
    public void drawSeries(Canvas canvas, GraphViewData[] values, float graphwidth, float graphheight, float border, double minX, double minY, double diffX, double diffY, float horstart) {
        // draw background
        double lastEndY = 0;
        double lastEndX = 0;
        if (drawBackground) {
            float startY = graphheight + border;
            for (int i = 0; i < values.length; i++) {
                boolean mustExit = false;
                double valY = values[i].valueY - minY;
                double ratY = valY / diffY;
                double y = graphheight * ratY;

                double valX = values[i].valueX - minX;
                double ratX = valX / diffX;
                double x = graphwidth * ratX;

                float endX = (float) x + (horstart + 1);
                float endY = (float) (border - y) + graphheight + 2;

                if (minX >= backgroundLimit)
                    break;

                if (i > 0) {
                    if (valX + minX >= backgroundLimit) {
                        double ne = (backgroundLimit - minX) / diffX * graphwidth;
                        endY = (float) (lastEndY + (endY - lastEndY) * (ne - lastEndX) / (endX - lastEndX));
                        endX = (float) (ne);
                        mustExit = true;
                    }

                    // fill space between last and current point
                    int numSpace = (int) ((endX - lastEndX) / 3f) + 1;
                    for (int xi = 0; xi < numSpace; xi++) {
                        float spaceX = (float) (lastEndX + ((endX - lastEndX) * xi / (numSpace - 1)));
                        float spaceY = (float) (lastEndY + ((endY - lastEndY) * xi / (numSpace - 1)));

                        // start => bottom edge
                        float startX = spaceX;

                        // do not draw over the left edge
                        if (startX - horstart > 1) {
                            canvas.drawLine(startX, startY, spaceX, spaceY, paintBackground);
                        }
                    }
                }

                lastEndY = endY;
                lastEndX = endX;

                if (mustExit)
                    break;
            }
        }

        // draw data
        lastEndY = 0;
        lastEndX = 0;
        for (int i = 0; i < values.length; i++) {
            double valY = values[i].valueY - minY;
            double ratY = valY / diffY;
            double y = graphheight * ratY;

            double valX = values[i].valueX - minX;
            double ratX = valX / diffX;
            double x = graphwidth * ratX;

            if (i > 0) {
                float startX = (float) lastEndX + (horstart + 1);
                float startY = (float) (border - lastEndY) + graphheight;
                float endX = (float) x + (horstart + 1);
                float endY = (float) (border - y) + graphheight;

                canvas.drawLine(startX, startY, endX, endY, paint);
            }
            lastEndY = y;
            lastEndX = x;
        }
    }

    public boolean getDrawBackground() {
        return drawBackground;
    }

    /**
     * @param drawBackground true for a light blue background under the graph line
     */
    public void setDrawBackground(boolean drawBackground) {
        this.drawBackground = drawBackground;
    }

    /**
     * @param stop drawing background at this limit
     */
    public void setDrawBackgroundLimit(float pos) {
        this.backgroundLimit = pos;
        viewGraph.invalidate();
    }

    public void load(Program pr) {
        if (this.pr == pr)
            return;
        this.pr = pr;
        title = pr.getName();
        long programLength = pr.getLength();

        int GRAPH_VOICE_SPAN = 600;

        Iterator<Period> iP = pr.getPeriodsIterator();
        int numPeriods = pr.getNumPeriods();
        GraphViewData data[] = new GraphViewData[numPeriods * 2];
        int i = 0;
        int cursor = 0;
        double maxFreq = 0;
        while (iP.hasNext()) {
            Period cP = iP.next();

            data[i++] = new GraphViewData(cursor + 0.01, cP.getMainBeatStart());
            cursor += cP.getLength();
            data[i++] = new GraphViewData(cursor, cP.getMainBeatEnd());

            maxFreq = Math.max(maxFreq, cP.getMainBeatStart());
            maxFreq = Math.max(maxFreq, cP.getMainBeatEnd());
        }

        graphSeries.clear();
        GraphViewSeries voiceSeries = new GraphViewSeries(data);
        addSeries(voiceSeries); // data

        setManualYAxisBounds(((int) Math.ceil(maxFreq)), 0);
        setScrollable(true);

        int viewstart = 0;
        int viewsize = (int) programLength; // (int) Math.min(programLength, GRAPH_VOICE_SPAN);
        setViewPort(viewstart, viewsize);
    }
}
